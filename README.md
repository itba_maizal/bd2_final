# bd2_final

Demo para plataforma para obtener información de componentes de computación y su relación de compatibilidad con productos complementarios

## Dependencias

Requiere de un ambiente para node.js (npm / yarn). Si decide utilizar las bases de datos provistas en este proyecto, se requiere adicionalmente de Docker y Docker Compose.

## Instalación

1- Clonar el repositorio

2- Instalar dependencias
```bash
# npm
cd ./backend && npm install && cd ..
cd ./website-demo/buscapiezas && npm install && cd .. && cd ..
npm install
```
```bash
# yarn
cd ./backend && yarn && cd ..
cd ./website-demo/buscapiezas && yarn && cd .. && cd ..
yarn
```

3- Modificar variables de entorno (Opcional)
Para modificar valores a utilizar como puertos y ubicaciones de las bases de datos
```bash
vi backend/.env
```

4- Descargar las bases de datos (Opcional)
Obligatorio si se opta por utilizar las bases de datos provistas por este repositorio
```bash
# npm
cd ./backend && npm db_up && cd ..
```
```bash
# yarn
cd ./backend && yarn db_up && cd ..
```

5- Lanzar el proyecto
```bash
# npm
npm start
```
```bash
# yarn
yarn start
```
## Autores
* Cristóbal Rojas
* Enrique Tawara
* Matías Ricarte