/**
 * API route for component
 */

const express = require("express");
const router = express.Router();

const mongo = require('./services/mongoDriver');
const ObjectId = require('mongodb').ObjectID;
const neo4j = require('./services/neo4jDriver');
const aux = require('./auxFunctions');

/**
 * @swagger
 * /api/component/by-name:
 *  get:
 *      tags:
 *          - Component
 *      description: Retorna un componente que coincida el nombre de uno existente en la base de datos
 *      produces:
 *          - application/json
 *      parameters:
 *          - name: name
 *            description: Nombre del componente a buscar
 *            type: string
 *            in: query
 *            required: true
 *          - name: compatibleinfo
 *            description: Si en el resultado se incluyen el id de los productos compatibles
 *            type: boolean
 *            in: query
 *            required: false
 *            default: false
 *          - name: compatibledetails
 *            description: Si en el resultado se incluyen los detalles de los productos compatibles. No hace nada si compatibleinfo es false.
 *            type: boolean
 *            in: query
 *            required: false
 *            default: false
 *      responses:
 *          200:
 *              description: Detalle del componente
 *          400:
 *              description: Valor inválido para nombre
 *          500:
 *              description: Error en el procesamiento
 */
router.get('/by-name', async (req, res) => {

    if(!req.query.name || !aux.isString(req.query.name)){
        res.status(400).jsonp({});
        return;
    }

    const name = req.query.name;
    const compatibleInfo = (req.query.compatibleinfo == 'true');
    const compatibleDetails = (req.query.compatibledetails == 'true');

    const mongoClient = mongo.getClient();
    const neo4jDriver = neo4j.getDriver();
    
    try {
        await mongoClient.connect();

        let result = await mongo.findOneDocumentByName(mongoClient,name);

        if(result === null){
            res.jsonp(result);
        }

        if(compatibleInfo){
            let relatedComponents = await neo4j.getCompatibleById(neo4jDriver,result._id);
            let relations = await aux.getRelatedComponents(relatedComponents,mongoClient,compatibleDetails);
            result['CompatibleCount'] = relations.length;
            result['CompatibleWith'] = relations;
        }

        res.jsonp(result);
     
    } catch (e) {
        console.error(e);
        res.status(500).jsonp({});
    } finally {
        await mongoClient.close();
        await neo4jDriver.close();
    }
});

/**
 * @swagger
 * /api/component/by-id:
 *  get:
 *      tags:
 *          - Component
 *      description: Retorna un componente que coincida el id de uno existente en la base de datos
 *      produces:
 *          - application/json
 *      parameters:
 *          - name: id
 *            description: Id del componente a buscar
 *            type: string
 *            in: query
 *            required: true
 *          - name: compatibleinfo
 *            description: Si en el resultado se incluyen el id de los productos compatibles
 *            type: boolean
 *            in: query
 *            required: false
 *            default: false
 *          - name: compatibledetails
 *            description: Si en el resultado se incluyen los detalles de los productos compatibles. No hace nada si compatibleinfo es false.
 *            type: boolean
 *            in: query
 *            required: false
 *            default: false
 *      responses:
 *          200:
 *              description: Detalle del componente
 *          400:
 *              description: Valor inválido para id
 *          500:
 *              description: Error en el procesamiento
 */
router.get('/by-id', async (req, res) => {

    if(!req.query.id || !aux.isString(req.query.id)){
        res.status(400).jsonp({});
        return;
    }

    const id = req.query.id;
    const compatibleInfo = (req.query.compatibleinfo == 'true');
    const compatibleDetails = (req.query.compatibledetails == 'true');

    const mongoClient = mongo.getClient();
    const neo4jDriver = neo4j.getDriver();
    
    try {
        await mongoClient.connect();

        let result = await mongo.findOneDocumentById(mongoClient,id);

        if(!result){
            res.status(400).jsonp({});
            return;
        }

        if(compatibleInfo){
            let relatedComponents = await neo4j.getCompatibleById(neo4jDriver,id);
            let relations = await aux.getRelatedComponents(relatedComponents,mongoClient,compatibleDetails);
            result['CompatibleCount'] = relations.length;
            result['CompatibleWith'] = relations;
        }

        res.jsonp(result);
     
    } catch (e) {
        console.error(e);
        res.status(500).jsonp({});
    } finally {
        await mongoClient.close();
        await neo4jDriver.close();
    }
});

module.exports = router;