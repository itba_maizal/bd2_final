// auxiliar functions
const mongo = require('./services/mongoDriver');
const ObjectId = require('mongodb').ObjectID;
const neo4j = require('./services/neo4jDriver');

async function getRelatedComponents(relatedComponents,mongoClient,compatibleDetails){

    let relations = [];
    if(relatedComponents !== null){
        if(compatibleDetails){
            await Promise.all(relatedComponents.map(async (element) => {
                elementId = element.get('node').properties._id;
                const componentNode = await mongo.findOneDocumentById(mongoClient,elementId);
                let obj = {_id: componentNode._id,Component: componentNode};
                if(element.get('node').properties.ct){
                    obj['count'] = element.get('node').properties.ct.low;
                }
                relations.push(obj);
              }));
        }else{
            relatedComponents.forEach(element => {
                elementId = element.get('node').properties._id;
                let obj = {_id: elementId};
                if(element.get('node').properties.ct){
                    obj['count'] = element.get('node').properties.ct.low;
                }
                relations.push(obj);
            })
        }
    }
    return relations;
}

function isString(object){
    return (typeof object === 'string' || object instanceof String)
}

// export functions
module.exports = {
    getRelatedComponents,
    isString
};