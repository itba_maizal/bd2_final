// all mongo related functions available
const {MongoClient, ObjectID} = require('mongodb');
const ObjectId = require('mongodb').ObjectID;

let mongoUser = "";
if(process.env.MONGO_USER_NAME.length >0){
    mongoUser += process.env.MONGO_USER_NAME;
    if(process.env.MONGO_USER_PASS.length >0){
        mongoUser += (':'+process.env.MONGO_USER_PASS);
    }
    mongoUser += '@'
}
const MONGO_HOST = process.env.MONGO_HOST || '0.0.0.0';
const MONGO_PORT = process.env.MONGO_PORT || 27017; 
const MONGO_URI = "mongodb://"+mongoUser+MONGO_HOST+":"+MONGO_PORT+"/test?retryWrites=true&w=majority&authSource=admin";

const MAX_DOCUMENTS_PER_PAGE = 50;

function getClient(){
    return new MongoClient(MONGO_URI,{ useNewUrlParser: true, useUnifiedTopology: true }, { keepAlive: 1});
}

async function getDocumentCount(client,query={}){
    const result = await client.db(process.env.MONGO_DB).collection(process.env.MONGO_COLLECTION).countDocuments(query);
    return result;
}

async function getAllDocuments(client){
    const cursor = await client.db(process.env.MONGO_DB).collection(process.env.MONGO_COLLECTION).find({}).sort({"_id":1});
    return await cursor.toArray();
}

async function getDocuments(client,pageNo=1,query={},sort="_id",sortOrder=1){

    const pageLimits = await getPageLimits(client);
    let resultList = [];
    const sortCriteria ={};
    sortCriteria[sort] = sortOrder;

    if(pageNo <= pageLimits.max){
        const cursor = await client.db(process.env.MONGO_DB).collection(process.env.MONGO_COLLECTION).find(query)
        .sort(sortCriteria)
        .skip(pageNo > 0 ? ( ( pageNo - 1 ) * MAX_DOCUMENTS_PER_PAGE) : 0)
        .limit(MAX_DOCUMENTS_PER_PAGE);

        resultList = await cursor.toArray();
    }

    const min = pageLimits['min'];
    const len = resultList.length;
    const max = pageLimits['max'];

    let result = {
        header: {
            first: min,
            current: pageNo,
            count: len,
            last: max,
        },
        result: resultList
    };

    if(pageNo > min && pageNo <= max){
        result['header']['prev'] = pageNo -1;
    }
    if(pageNo => min && pageNo < max){
        result['header']['next'] = pageNo +1;
    }

    return result;
}

async function findOneDocumentById(client, id){

    try{
        objectId = ObjectId(id);
    }catch(e){
        return null;
    }
    

    const result = await client.db(process.env.MONGO_DB).collection(process.env.MONGO_COLLECTION).findOne({'_id': objectId});
    return result;
}

async function findOneDocumentByName(client, name){
    const result = await client.db(process.env.MONGO_DB).collection(process.env.MONGO_COLLECTION).findOne({'Name': name});
    return result;
}

async function getFieldNameList(client,query={}){
    const pipeline = [ {"$match": query}, {"$project":{"arrayofkeyvalue":{"$objectToArray":"$$ROOT"}}}, {"$unwind":"$arrayofkeyvalue"}, {"$group":{"_id":null,"fields":{"$addToSet":"$arrayofkeyvalue.k"}}} ];
    const cursor = await client.db(process.env.MONGO_DB).collection(process.env.MONGO_COLLECTION).aggregate(pipeline);

    return (await cursor.toArray())[0]['fields'];
}

async function getFieldNameValueList(client,fieldName,query){
    const result = await client.db(process.env.MONGO_DB).collection(process.env.MONGO_COLLECTION).distinct(fieldName,query);
    return result;
}

// auxiliar functions

async function getPageLimits(client){
    const documentCount = await getDocumentCount(client);
    return {
        min: 1,
        max: parseInt(documentCount/MAX_DOCUMENTS_PER_PAGE)+1
    };
}

// export functions
module.exports = {
    getClient,
    getDocumentCount,
    getAllDocuments,
    getDocuments,
    findOneDocumentById,
    findOneDocumentByName,
    getFieldNameList,
    getFieldNameValueList
};