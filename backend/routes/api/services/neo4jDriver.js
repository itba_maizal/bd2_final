const neo4j = require('neo4j-driver');

const NEO4J_HOST = process.env.NEO4J_HOST || '0.0.0.0';
const NEO4J_PORT = process.env.NEO4J_PORT_BOLT || 7687; 
const NEO4J_URI = "bolt://"+NEO4J_HOST+":"+NEO4J_PORT;

let neo4jUser = neo4j.auth.basic("neo4j","");
if(process.env.NEO4J_USER_NAME.length >0 && process.env.NEO4J_USER_PASS.length >0){
    neo4jUser = neo4j.auth.basic(process.env.NEO4J_USER_NAME,process.env.NEO4J_USER_PASS);
}

function getDriver(){
    return neo4j.driver(NEO4J_URI, neo4jUser);
}

async function getCompatibleById(driver, id){

    if(!id){
        return null;
    }

    const cypher = 'MATCH (this:Component {_id: $cid})-[relationship:COMPATIBLE_WITH]-(node:Component) RETURN relationship,node';
    const params = {cid: id.toString()};

    return await runReadQuery(driver, cypher, params);
}

async function getAllCompatibleByIds(driver, ids, category="",limit=1){

    if(!ids || ids.length<1 || Number.isNaN(limit) || !Number.isInteger(limit) || limit < 1 ){
        return null;
    }

    const cypher = 'MATCH (this:Component)-[relationship:COMPATIBLE_WITH]-(node:Component) WHERE ALL( x IN this._id WHERE x IN $param) ' +
                    ((category && category.length > 0)?('AND node.Category=$category '):('')) +
                    'WITH DISTINCT node AS n WITH {_id:n._id} AS properties ' +
                    'RETURN {properties: properties} AS node ' +
                    'LIMIT toInteger($limit)';
    let params = {
        param: ids,
        limit: limit
    };
    if(category && category.length > 0){
        params['category'] = category;
    }

    return await runReadQuery(driver, cypher, params);
}

async function getAnyCompatibleByIds(driver, ids, category="",limit=1){

    if(!ids || ids.length<1 || Number.isNaN(limit) || !Number.isInteger(limit) || limit < 1 ){
        return null;
    }

    const cypher = 'MATCH (this:Component)-[relationship:COMPATIBLE_WITH]-(node:Component) WHERE ANY( x IN this._id WHERE x IN $param) ' +
                    ((category && category.length > 0)?('AND node.Category=$category '):('')) +
                    'WITH DISTINCT node AS n, count(this) AS ct ' +
                    'WITH {_id:n._id, ct:toInteger(ct)} AS properties ' +
                    'RETURN {properties: properties} AS node ' +
                    'ORDER BY properties.ct DESC LIMIT toInteger($limit)';
    let params = {
        param: ids,
        limit: limit
    };
    if(category && category.length > 0){
        params['category'] = category;
    }

    return await runReadQuery(driver, cypher, params);
}

async function getComponentsByHighCompatibility(driver, category, ids={}, limit=1){
    if(Number.isNaN(limit) || !Number.isInteger(limit) || limit < 1 ){
        return null;
    }

    let cypher = '';

    if(ids && ids.length > 0){
        cypher +=   'MATCH (o:Component)-[:COMPATIBLE_WITH]-(e:Component)' +
                    'WHERE ALL( x IN e._id WHERE x IN $param)' +
                    'WITH DISTINCT o AS o' +
                    'MATCH (n:Component {Category: $category})-[:COMPATIBLE_WITH]-(o)' +
                    'WITH DISTINCT n AS n' +
                    'MATCH (n)-[:COMPATIBLE_WITH]-(e)' +
                    'WITH DISTINCT n, {_id:n._id, ct:toInteger(count(e))} AS properties' +
                    'RETURN {properties: properties} AS node' +
                    'ORDER BY properties.ct DESC LIMIT toInteger($limit)';

        cypher += 'MATCH (o:Component)-[:COMPATIBLE_WITH]-(e:Component)-[:COMPATIBLE_WITH]-(n:Component {Category: $category}) WHERE ALL( x IN e._id WHERE x IN $param) ';
        cypher += 'WITH DISTINCT n AS n,o, count(o) AS ct MATCH (n)-[:COMPATIBLE_WITH]-(o) '
    }else{
        cypher += 'MATCH (n:Component {Category: $category})-[:COMPATIBLE_WITH]-(o:Component) WITH DISTINCT n AS n, count(o) AS ct ';
        cypher  +=  'WITH DISTINCT n AS n, count(o) AS ct ' +
                'WITH {_id:n._id, ct:toInteger(ct)} AS properties ' +
                'RETURN {properties: properties} AS node ' +
                'ORDER BY properties.ct DESC LIMIT toInteger($limit)';
    }

    let params = {
                    limit: limit,
                    category: category
                    };
    if(ids && ids.length > 0){
        params['param'] = ids;
    }

    return await runReadQuery(driver, cypher, params);
}

// auxiliar functions
async function runReadQuery(driver, cypher, params){

    let res = null;

    const session = driver.session({ defaultAccessMode: neo4j.session.READ });

    await session.run(cypher,params)
    .then(result => {
        if(result.records){
            res = result.records;
        }
    })
    .catch(e => {
        // Output the error
        console.log(e);
    })
    .then(() => {
        // Close the Session
        return session.close();
    })

    return res;
}

// export functions
module.exports = {
    getDriver,
    getCompatibleById,
    getAllCompatibleByIds,
    getAnyCompatibleByIds,
    getComponentsByHighCompatibility
};