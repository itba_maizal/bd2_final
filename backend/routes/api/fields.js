/**
 * API route for fields
 */

const express = require("express");
const router = express.Router();

const mongo = require('./services/mongoDriver');
const ObjectId = require('mongodb').ObjectID;
const neo4j = require('./services/neo4jDriver');
const aux = require('./auxFunctions');

/**
 * @swagger
 * /api/fields/get-list:
 *  get:
 *      tags:
 *          - Fields
 *      description: Retorna un arreglo con los distintos campos posibles
 *      produces:
 *          - application/json
 *      parameters:
 *          - name: filter
 *            description: Colección de clave valor para filtrar
 *            type: object
 *            additionalProperties: true
 *            in: query
 *            example: '{"Category": "Motherboard"}'
 *      responses:
 *          200:
 *              description: Arreglo con los nombres de los campos
 *          400:
 *              description: Formato inválido para filtro
 *          500:
 *              description: Error en el procesamiento
 */
router.get('/get-list', async (req, res) => {

    let filter;
    try{
        filter = (req.query.filter)?(JSON.parse(req.query.filter)):({});
    }catch (e){ 
        console.error(e);
        res.status(400).jsonp({});
        return;
    }

    const mongoClient = mongo.getClient();
    
    try {
        await mongoClient.connect();

        let result = await mongo.getFieldNameList(mongoClient,filter);

        res.jsonp(result);
     
    } catch (e) {
        console.error(e);
        res.status(500).jsonp({});
    } finally {
        await mongoClient.close();
    }
});

/**
 * @swagger
 * /api/fields/get-values-list:
 *  get:
 *      tags:
 *          - Fields
 *      description: Retorna un arreglo con los distintos valores para un posible campo. Retorna la lista de categorias por defecto.
 *      produces:
 *          - application/json
 *      parameters:
 *          - name: filter
 *            description: Colección de clave valor para filtrar
 *            type: object
 *            additionalProperties: true
 *            in: query
 *            example: '{"Category": "Motherboard"}'
 *            default: '{}'
 *          - name: field
 *            description: Campo por cual se busca los valores
 *            type: string
 *            in: query
 *            example: 'Category'
 *            default: 'Category'
 *      responses:
 *          200:
 *              description: Arreglo con los posibles valores para el campo
 *          400:
 *              description: Formato inválido para filtro
 *          500:
 *              description: Error en el procesamiento
 */
router.get('/get-values-list', async (req, res) => {

    let filter;
    try{
        filter = (req.query.filter)?(JSON.parse(req.query.filter)):({});
    }catch (e){ 
        console.error(e);
        res.status(400).jsonp({});
        return;
    }
    const field = req.query.field || "Category";
    if (!aux.isString(field)){
        res.status(400).jsonp({});
        return;
    }

    const mongoClient = mongo.getClient();
    
    try {
        await mongoClient.connect();

        let result = await mongo.getFieldNameValueList(mongoClient,field,filter);

        res.jsonp(result);
     
    } catch (e) {
        console.error(e);
        res.status(500).jsonp({});
    } finally {
        await mongoClient.close();
    }
});


module.exports = router;