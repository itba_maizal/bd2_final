/**
 * API route for components
 */

const express = require("express");
const router = express.Router();

const mongo = require('./services/mongoDriver');
const ObjectId = require('mongodb').ObjectID;
const neo4j = require('./services/neo4jDriver');
const aux = require('./auxFunctions');

/**
 * @swagger
 * /api/components:
 *  get:
 *      tags:
 *          - Components
 *      description: Retorna una lista de componentes con paginación
 *      produces:
 *          - application/json
 *      parameters:
 *          - name: page
 *            description: Página de la lista de componentes
 *            type: integer
 *            in: query
 *            required: false
 *            default: 1
 *            min: 1
 *          - name: sort
 *            description: Por que criterio ordenar
 *            type: string
 *            in: query
 *            required: false
 *            default: _id
 *          - name: asc
 *            description: Si ordenar por ascendiente o descendiente
 *            type: boolean
 *            in: query
 *            required: false
 *            default: true
 *          - name: filter
 *            description: Colección de clave valor para filtrar
 *            type: object
 *            additionalProperties: true
 *            in: query
 *            example: '{"Category": "Motherboard"}'
 *          - name: noheader
 *            description: Para indicar si la información del header de la respuesta debe figurar en el cuerpo. Para casos donde el header de la respuesta no es accesible
 *            type: boolean
 *            in: query
 *            required: false
 *            default: false
 *      responses:
 *          200:
 *              description: JSON array de componentes
 *          400:
 *              description: Valor inválido para página o filtro
 *          500:
 *              description: Error en el procesamiento
 */
router.get('/', async (req, res) => {

    let filter;
    try{
        filter = (req.query.filter)?(JSON.parse(req.query.filter)):({});
    }catch (e){ 
        console.error(e);
        res.status(400).jsonp({});
        return;
    }

    let noheader = (req.query.noheader === "true");

    let page = req.query.page || 1;
    let sort = req.query.sort || "_id";
    let order = (req.query.asc == 'true')?(1):(-1);
    
    if(isNaN(page) || Number(page)<1 || !aux.isString(sort)){
        res.status(400).jsonp({});
        return;
    }

    page = parseInt(page);

    const mongoClient = mongo.getClient();
    
    try {
        await mongoClient.connect();

        let result = await mongo.getDocuments(mongoClient,page,filter,sort,order);

        if(noheader){
            // for json with header on result, in case headers are unavailable
            res.jsonp(result);
        }else{
            // Implementation with data in header
            res.header('Count',result['header']['count']);
            res.header('First',result['header']['first']);
            if(result['header']['prev']){
                res.header('Prev',result['header']['prev']);
            }
            res.header('Current',result['header']['current']);
            if(result['header']['last']){
                res.header('Next',result['header']['next']);
            }
            res.header('Last',result['header']['last']);
            
            res.jsonp(result['result']);
        }

        return;
     
    } catch (e) {
        console.error(e);
        res.status(500).jsonp({});
    } finally {
        await mongoClient.close();
    }
});

/**
 * @swagger
 * /api/components/all:
 *  get:
 *      tags:
 *          - Components
 *      description: Retorna una lista con todos los componentes en la base de datos, sin paginación
 *      produces:
 *          - application/json
 *      responses:
 *          200:
 *              description: Arreglo JSON con todos los componentes
 *          500:
 *              description: Error en el procesamiento
 */
router.get('/all', async (req, res) => {

    const mongoClient = mongo.getClient();

    try {
        await mongoClient.connect();
        let result = await mongo.getAllDocuments(mongoClient);
        res.jsonp(result);
        return;
     
    } catch (e) {
        console.error(e);
        res.status(500).jsonp({});
    } finally {
        await mongoClient.close();
    }
});

/**
 * @swagger
 * /api/components/count:
 *  get:
 *      tags:
 *          - Components
 *      description: Retorna la cantidad total de componentes
 *      produces:
 *          - application/json
 *      parameters:
 *          - name: filter
 *            description: Colección de clave valor para filtrar
 *            type: object
 *            additionalProperties: true
 *            in: query
 *            example: '{"Category": "Motherboard"}'
 *      responses:
 *          200:
 *              description: Cantidad de componentes
 *          400:
 *              description: Valor inválido para filtro
 *          500:
 *              description: Error en el procesamiento
 */
router.get('/count', async (req, res) => {

    let filter;
    try{
        filter = (req.query.filter)?(JSON.parse(req.query.filter)):({});
    }catch (e){ 
        console.error(e);
        res.status(400).jsonp({});
        return;
    }

    const mongoClient = mongo.getClient();
    
    try {
        await mongoClient.connect();

        let result = await mongo.getDocumentCount(mongoClient,filter);

        res.jsonp({count: result});
     
    } catch (e) {
        console.error(e);
        res.status(500).jsonp({});
    } finally {
        await mongoClient.close();
    }
});

/**
 * @swagger
 * /api/components/compatible-by-ids:
 *  get:
 *      tags:
 *          - Components
 *      description: Retorna una lista de componentes que son compatibles con los componentes ingresados
 *      produces:
 *          - application/json
 *      parameters:
 *          - name: id
 *            description: Id del componente de interes. Soporta varios valores. Mínimo 1 valor
 *            type: array
 *            collectionFormat: multi
 *            items:
 *              type: string
 *            in: query
 *            required: true
 *          - name: predicate
 *            description: Acepta solamente ANY o ALL (o vacío, que se comporta como ANY). Indica el comportamiento, si la lista de componentes resultante debe ser compatible con ALGUNO (ANY) de los componentes ingresados o con TODOS (ALL).
 *            type: string
 *            in: query
 *            required: false
 *            default: ANY
 *          - name: category
 *            description: El tipo de componente de los componentes de la lista resultante
 *            type: string
 *            in: query
 *            required: false
 *          - name: compatibledetails
 *            description: Si en el resultado se incluyen los detalles de los productos compatibles.
 *            type: boolean
 *            in: query
 *            required: false
 *            default: false
 *          - name: limit
 *            description: Limita la cantidad de resultados.
 *            type: integer
 *            in: query
 *            required: false
 *            default: 1
 *            min: 1
 *      responses:
 *          200:
 *              description: JSON array de componentes compatibles con los ingresados
 *          400:
 *              description: Valores inválidos para id o predicate
 *          500:
 *              description: Error en el procesamiento
 */
router.get('/compatible-by-ids', async (req, res) => {

    let limit = req.query.limit || 1;
    let category = req.query.category || "";
    const compatibleDetails = (req.query.compatibledetails == 'true');
    const predicate = ((!req.query.predicate || req.query.predicate.toUpperCase() == 'ANY')?('ANY'):((req.query.predicate.toUpperCase() == 'ALL')?('ALL'):('')));

    if(isNaN(limit) || !req.query.id || req.query.id.length==0 || predicate.length==0 || !aux.isString(category)){
        res.status(400).jsonp({});
        return;
    }

    limit = Number.parseInt(limit);

    let ids = req.query.id;
    if(!Array.isArray(ids)){
        ids = [ ids ];
    }

    const mongoClient = mongo.getClient();
    const neo4jDriver = neo4j.getDriver();

    try {
        await mongoClient.connect();

        let components;
        if(predicate == 'ANY'){
            components = await neo4j.getAnyCompatibleByIds(neo4jDriver,ids,category,limit);
        }else{
            components = await neo4j.getAllCompatibleByIds(neo4jDriver,ids,category,limit);
        }

        if(!components){
            res.status(400).jsonp({});
            return;
        }

        let result = await aux.getRelatedComponents(components,mongoClient,compatibleDetails);

        res.header('Count',result.length);

        res.jsonp(result);
     
    } catch (e) {
        console.error(e);
        res.status(500).jsonp({});
    } finally {
        await mongoClient.close();
        await neo4jDriver.close();
    }
});

/**
 * @swagger
 * /api/components/by-high-compatibility:
 *  get:
 *      tags:
 *          - Components
 *      description: Retorna una lista con los componentes más compatibles de la categoria
 *      produces:
 *          - application/json
 *      parameters:
 *          - name: id
 *            description: Id del componente de interes. Soporta varios valores. No recomendado usar actualmente (por baja memoria)
 *            type: array
 *            collectionFormat: multi
 *            items:
 *              type: string
 *            in: query
 *            required: false
 *          - name: category
 *            description: El tipo de componente de los componentes de la lista resultante
 *            type: string
 *            in: query
 *            required: true
 *          - name: compatibledetails
 *            description: Si en el resultado se incluyen los detalles de los productos compatibles.
 *            type: boolean
 *            in: query
 *            required: false
 *            default: false
 *          - name: limit
 *            description: Limita la cantidad de resultados.
 *            type: integer
 *            in: query
 *            required: false
 *            default: 1
 *            min: 1
 *      responses:
 *          200:
 *              description: JSON array de componentes compatibles con los ingresados
 *          400:
 *              description: Valores inválidos para id, limit o category
 *          500:
 *              description: Error en el procesamiento
 */
router.get('/by-high-compatibility', async (req, res) => {

    let limit = req.query.limit || 1;
    let category = req.query.category || "";
    const compatibleDetails = (req.query.compatibledetails == 'true');

    if(isNaN(limit) || !aux.isString(category)){
        res.status(400).jsonp({});
        return;
    }

    limit = Number(limit);

    let ids = req.query.id;
    if(ids && !Array.isArray(ids)){
        ids = [ ids ];
    }

    const mongoClient = mongo.getClient();
    const neo4jDriver = neo4j.getDriver();

    try {
        await mongoClient.connect();

        const components = await neo4j.getComponentsByHighCompatibility(neo4jDriver,category,ids,limit);

        if(!components){
            res.status(400).jsonp({});
            return;
        }

        let result = await aux.getRelatedComponents(components,mongoClient,compatibleDetails);

        res.header('Count',result.length);

        res.jsonp(result);
     
    } catch (e) {
        console.error(e);
        res.status(500).jsonp({});
    } finally {
        await mongoClient.close();
        await neo4jDriver.close();
    }
});

module.exports = router;