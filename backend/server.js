//Dependencies
const express = require('express');
let cors = require('cors');
const bodyParser = require('body-parser');
require('dotenv').config();

const API_HOST = process.env.API_HOST || '0.0.0.0';
const API_PORT = process.env.API_PORT || 3001; 

const app = express();
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(cors());

const swaggerJsonDoc = require('swagger-jsdoc')
const swaggerUI = require('swagger-ui-express')

const swaggerOptions = {
	swaggerDefinition: {
		info: {
			title: "Components API",
			description: "Documentación en SWAGGER de la API",
			servers: ['http://localhost:3001']
		}
	},
	apis: [
		'./routes/api/components.js',
		'./routes/api/component.js',
		'./routes/api/fields.js'
	]
}

const swaggerDocs = swaggerJsonDoc(swaggerOptions)
app.use('/api/swagger', swaggerUI.serve, swaggerUI.setup(swaggerDocs))

app.use("/api/components", require('./routes/api/components.js'))
app.use("/api/component", require('./routes/api/component.js'))
app.use("/api/fields", require('./routes/api/fields.js'))

// listen on port
app.listen(API_PORT,API_HOST, () => console.log(`API LISTENING ON http://${API_HOST}:${API_PORT}`));