import json
import re


def is_compatible_cpu_mobo(cpu, mobo):
    if 'Socket / CPU' not in mobo:
        return False
    if 'Socket' not in cpu:
        return False
    socket = re.split(r'[/ ]', mobo['Socket / CPU'])
    if cpu['Socket'] in socket:
        return True
    return False


def is_compatible_cpu_cooler(cpu, cooler):
    if 'Socket' not in cpu:
        return False
    if 'CPU Socket' not in cooler:
        return False
    socket = cooler['CPU Socket'].split('\n')
    if cpu['Socket'] in socket:
        return True
    return False


def is_compatible_mobo_cooler(mobo, cooler):
    if 'Socket / CPU' not in mobo:
        return False
    if 'CPU Socket' not in cooler:
        return False
    mobo_sockets = re.split(r'[/ ]', mobo['Socket / CPU'])
    cooler_sockets = cooler['CPU Socket'].split('\n')
    for socket in mobo_sockets:
        if socket in cooler_sockets:
            return True
    return False


def is_compatible_mobo_ram(mobo, ram):
    if 'Memory Type' not in mobo:
        return False
    if 'Form Factor' not in ram:
        return False
    types = mobo['Memory Type'].split('\n')
    form_factor = ram['Form Factor']
    if 'DDR2' in types and form_factor == '200-pin SODIMM':
        return True
    elif 'DDR3' in types and form_factor == '204-pin SODIMM':
        return True
    elif 'DDR4' in types and form_factor == '260-pin SODIMM':
        return True
    return False


def is_compatible_mobo_storage(mobo, storage):
    if 'Interface' not in storage:
        return False
    interface = storage['Interface'].split()

    # First we check the SATA stuff
    if interface[0] == "SATA":
        # We need at least a sata 1.5 Gb/s for it to work
        if 'SATA 6 Gb/s' in mobo and int(mobo['SATA 6 Gb/s']) > 0:
            return True
        elif 'SATA 3 Gb/s' in mobo and int(mobo['SATA 3 Gb/s']) > 0:
            return True
        elif 'SATA 1.5 Gb/s' in mobo and int(mobo['SATA 1.5 Gb/s']) > 0:
            return True
    elif interface[0] == 'PATA' or interface[0] == 'SAS':
        # We search literally
        aux = storage['Interface']
        if aux in mobo and int(mobo[aux]) > 0:
            return True
    elif interface[0] == 'Micro' and interface[1] == 'SATA':
        if 'Micro SATA 6 Gb/s' in mobo and int(mobo['Micro SATA 6 Gb/s']) > 0:
            return True
        elif 'Micro SATA 3 Gb/s' in mobo and int(mobo['Micro SATA 3 Gb/s']) > 0:
            return True
    elif interface[0] == 'mSATA':
        if 'mSATA Slots' in mobo and int(mobo['mSATA Slots']) > 0:
            return True
    elif interface[0] == 'PCIe':
        if 'PCI-E x16 Slots' in mobo and int(mobo['PCI-E x16 Slots']) > 0:
            return True
        elif interface[1] == 'x16':
            return False
        elif 'PCI-E x8 Slots' in mobo and int(mobo['PCI-E x8 Slots']) > 0:
            return True
        elif interface[1] == 'x8':
            return False
        elif 'PCI-E x4 Slots' in mobo and int(mobo['PCI-E x4 Slots']) > 0:
            return True
        elif interface[1] == 'x4':
            return False
        elif 'PCI-E x1 Slots' in mobo and int(mobo['PCI-E x1 Slots']) > 0:
            return True
    elif interface[0] == 'M.2':
        if 'M.2 Slots' in mobo and 'M-key' in mobo['M.2 Slots']:
            return True
    elif interface[0] == 'U.2':
        if 'U.2' in mobo and int(mobo['U.2']) > 0:
            return True
    return False


def is_compatible_mobo_gpu(mobo, gpu):
    if 'Interface' not in gpu:
        return False

    interface = gpu['Interface'].split()
    if interface[0] == 'PCI':
        if 'PCI Slots' in mobo and int(mobo['PCI Slots']) > 0:
            return True
    elif interface[0] == 'AGP':
        return False
    elif interface[0] == 'PCIe':
        if 'PCI-E x16 Slots' in mobo and int(mobo['PCI-E x16 Slots']) > 0:
            return True
        elif interface[1] == 'x16':
            return False
        elif 'PCI-E x8 Slots' in mobo and int(mobo['PCI-E x8 Slots']) > 0:
            return True
        elif interface[1] == 'x8':
            return False
        elif 'PCI-E x4 Slots' in mobo and int(mobo['PCI-E x4 Slots']) > 0:
            return True
        elif interface[1] == 'x4':
            return False
        elif 'PCI-E x1 Slots' in mobo and int(mobo['PCI-E x1 Slots']) > 0:
            return True
    return False


def is_compatible_mobo_case(mobo, case):
    if 'Motherboard Form Factor' not in case:
        return False
    if 'Form Factor' not in mobo:
        return False
    supported = case['Motherboard Form Factor'].split('\n')
    if mobo['Form Factor'] in supported:
        return True
    return False


def is_compatible_gpu_case(gpu, case):
    if 'Maximum Video Card Length' not in case:
        return False
    if 'Length' not in gpu:
        return False

    max_lengths = case['Maximum Video Card Length'].split('\n')
    card_length = gpu['Length'].split()[0]

    for x in max_lengths:
        max_size = x.split()[0]
        if float(card_length) <= float(max_size):
            return True
    return False


def is_compatible(obj1, obj2):
    cat1 = obj1['Category']
    cat2 = obj2['Category']
    if cat1 == cat2:
        return False
    elif cat1 == 'CPU' and cat2 == 'Motherboard':
        return is_compatible_cpu_mobo(obj1, obj2)
    elif cat2 == 'CPU' and cat1 == 'Motherboard':
        return is_compatible_cpu_mobo(obj2, obj1)
    elif cat1 == 'CPU' and cat2 == 'CPU Cooler':
        return is_compatible_cpu_cooler(obj1, obj2)
    elif cat2 == 'CPU' and cat1 == 'CPU Cooler':
        return is_compatible_cpu_cooler(obj2, obj1)
    elif cat1 == 'Motherboard' and cat2 == 'CPU Cooler':
        return is_compatible_mobo_cooler(obj1, obj2)
    elif cat2 == 'Motherboard' and cat1 == 'CPU Cooler':
        return is_compatible_mobo_cooler(obj2, obj1)
    elif cat1 == 'Motherboard' and cat2 == 'Memory':
        return is_compatible_mobo_ram(obj1, obj2)
    elif cat2 == 'Motherboard' and cat1 == 'Memory':
        return is_compatible_mobo_ram(obj2, obj1)
    elif cat1 == 'Motherboard' and cat2 == 'Storage':
        return is_compatible_mobo_storage(obj1, obj2)
    elif cat2 == 'Motherboard' and cat1 == 'Storage':
        return is_compatible_mobo_storage(obj2, obj1)
    elif cat1 == 'Motherboard' and cat2 == 'Video Card':
        return is_compatible_mobo_gpu(obj1, obj2)
    elif cat2 == 'Motherboard' and cat1 == 'Video Card':
        return is_compatible_mobo_gpu(obj2, obj1)
    elif cat1 == 'Motherboard' and cat2 == 'Case':
        return is_compatible_mobo_case(obj1, obj2)
    elif cat2 == 'Motherboard' and cat1 == 'Case':
        return is_compatible_mobo_case(obj2, obj1)
    elif cat1 == 'Video Card' and cat2 == 'Case':
        return is_compatible_gpu_case(obj1, obj2)
    elif cat2 == 'Video Card' and cat1 == 'Case':
        return is_compatible_gpu_case(obj2, obj1)

    return False


def parse_json(data_path, outputfile):
    # Opening JSON file
    with open(data_path) as json_file:
        data = json.load(json_file)
        f = open(outputfile, 'w')
        f.write('componente 1,componente 2\n')
        i = 0
        json_len = len(data)
        while i < json_len:
            j = i + 1
            print('Working')
            while j < json_len:
                if is_compatible(data[i], data[j]):
                    f.write(str(data[i]['_id']))
                    f.write(',')
                    f.write(str(data[j]['_id']))
                    f.write('\n')
                j = j + 1
            i = i + 1
        f.close()


if __name__ == '__main__':
    parse_json('data.json', 'out.csv')
