import ApiService from './ApiService'

export default class RestClient {

    async Get (url) {
        return await ApiService.get(url)
    }

    async Post (url, data) {
        return await ApiService.post(url, data)
    }

    Put (url, data) {
        return ApiService.put(url, data)
    }

    Delete (url) {
        return ApiService.delete(url)
    }
}
