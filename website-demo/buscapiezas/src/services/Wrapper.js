import RestClient from './RestClient'
export default class Wrapper {

    static getBaseUrl() {
        return 'http://localhost:3001/api/'
    }
    static acceptedCategories = ["CPU", "CPU Cooler", "Case", "Memory", "Motherboard", "Power Supply", "Storage", "Video Card"];
    static storageEntries = ['cpu', 'cpucooler', 'case', 'memory', 'motherboard', 'powersupply', 'storage', 'videocard'];

    //wth is a cookie????
    static storage = {
        get memory() {
            return localStorage.getItem('memory')
        },
        set memory(value) {
            localStorage.setItem('memory', value)
        },

        get storage() {
            return localStorage.getItem('storage')
        },
        set storage(value) {
            localStorage.setItem('storage', value)
        },

        get motherboard() {
            return localStorage.getItem('motherboard')
        },
        set motherboard(value) {
            localStorage.setItem('motherboard', value)
        },

        get videocard() {
            return localStorage.getItem('gpu')
        },
        set videocard(value) {
            localStorage.setItem('gpu', value)
        },

        get powersupply() {
            return localStorage.getItem('psu')
        },
        set powersupply(value) {
            localStorage.setItem('psu', value)
        },

        get cpu() {
            return localStorage.getItem('cpu')
        },
        set cpu(value) {
            localStorage.setItem('cpu', value)
        },

        get cpucooler() {
            return localStorage.getItem('cpucooler')
        },
        set cpucooler(value) {
            localStorage.setItem('cpucooler', value)
        },

        get case() {
            return localStorage.getItem('case')
        },
        set case(value) {
            localStorage.setItem('case', value)
        },
    }


    // all of the urls for calling the API

    static getMemoryUrl() {
        return this.getBaseUrl() + 'Memory/'
    }

    static getMotherboardUrl() {
        return this.getBaseUrl() + 'Motherboard/'
    }

    static async GetMemories() {
        return await new RestClient().Get(this.getMemoryUrl());
    }

    static async GetComponentById(id) {
        return await new RestClient().Get(this.getBaseUrl() + 'component/by-id?id=' + id)
        .then(response => {return response.json();});
    }
    static async GetComponents(page, sortBy, order, filter) {

        let query = "&sort=";
        query += (sortBy == null ? "Name" : encodeURIComponent(sortBy)) + "&asc=";
        query += (order == null ? "true" : order) + "&filter=";

        /*
            filter::: {"Category": { "$in" : [ "CPU", "Motherboard" ] } }
            filter>>> %7B%22Category%22%3A%20%7B%20%22%24in%22%20%3A%20%5B%20%22CPU%22%2C%20%22Motherboard%22%20%5D%20%7D%20%7D
        */

        query += (filter == null ? "%7B%7D" : this.parseFilter(filter)) + "&noheader=true";

        let res = await new RestClient().Get(this.getBaseUrl() + 'components?page=' + page + query)
        .then(response => {
            return response
        });
        let resjson = await res.json();
        return resjson;
    }

    static async GetCompatibleWithId(listOfIds, category, predicate){
        /*
        **  /api/components/compatible-by-ids?id=87412541251&predicate=ALL&category=Video%20Card&compatibledetails=false
        */
        let query = "components/compatible-by-ids"
        let ids ="?";

        for(var idx in listOfIds){
            ids += "id=" + listOfIds[idx] + "&";
        }

        let res = await new RestClient().Get(this.getBaseUrl() + query + ids 
            + "predicate=" + predicate + "&category=" + encodeURIComponent(category) + "&compatibledetails=false")
        .then(response => {
            return response
        });
        let resjson = await res.json();
        return resjson;
    }

    static async GetManufacturers(category){
        /*
        **  http://localhost:3001/api/fields/get-values-list?filter=%7B%22Category%22%20%3A%20%22Video%20Card%22%7D&field=Manufacturer
        */
        let query =  "fields/get-values-list?filter=%7B%22Category%22%20%3A%20%22" + encodeURIComponent(category) + "%22%7D&field=Manufacturer"
        let res = await new RestClient().Get(this.getBaseUrl() + query)
        .then(response => {
            return response
        });
        let resjson = await res.json();
        return resjson;
    }
    static async GetValues(category, field){
        /*
        **  http://localhost:3001/api/fields/get-values-list?filter=%7B%22Category%22%20%3A%20%22Video%20Card%22%7D&field=Manufacturer
        */
        let query =  "fields/get-values-list?filter=%7B%22Category%22%20%3A%20%22" + encodeURIComponent(category) + "%22%7D&field=" + encodeURIComponent(field);
        let res = await new RestClient().Get(this.getBaseUrl() + query)
        .then(response => {
            return response
        });
        let resjson = await res.json();
        return resjson;
    }

    static parseFilter(filter){

        let string = JSON.stringify(filter);

        return encodeURIComponent(string);
    }
}