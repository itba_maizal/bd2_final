export default class ApiService {
    static RequestTypes = {
        PUT: 'PUT',
        GET: 'GET',
        POST: 'POST',
        DELETE: 'DELETE',
    }

    static get timeout() {
        return 20 * 1000
    }



    static async fetch(type, url, data) {
        var headers = this.generateHeaders()
        var init = {
            method: type,
            headers: headers,
            body: JSON.stringify(data),
        }


        return await fetch(url, init)
            .then(function (response) {
            return response;
        });
    }

    static generateHeaders() {
        var header = new Headers()

        header.append('Content-Type', 'application/json')

        return header
    }

    static async get(url) {
        return await ApiService.fetch(this.RequestTypes.GET, url, undefined)
    }

    static async post(url, data) {
        return await ApiService.fetch(this.RequestTypes.POST, url, data);
    }

    static put(url, data) {
        return ApiService.fetch(this.RequestTypes.PUT, url, data)
    }

    static delete(url) {
        return ApiService.fetch(this.RequestTypes.DELETE, url, undefined)
    }
}