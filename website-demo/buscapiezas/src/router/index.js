import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

export default new VueRouter({
  mode: 'hash',
  //base: process.env.BASE_URL,
  base: '/',
  routes: [
    {
      path: '/',
      component: () => import('@/views/Pages/Index'),
      children: [
        // Browse Components
        {
          name: 'Browse Components',
          path: '/',
          component: () => import('@/views/Main'),
        },
        // Component
        {
          name: 'Component',
          path: '/component/*',
          component: () => import('@/views/ComponentInfo'),
        },
        // System Builder
        {
          name: 'Builder',
          path: '/builder',
          component: () => import('@/views/SystemBuilder'),
        },
        // Vuetify
        {
          name: 'Vuetify',
          path: '/vuetify',
          component: () => import('@/views/Home'),
        },
        // About
        {
          name: 'About',
          path: '/about',
          component: () => import('@/views/About')
        },
      ],
    },
  ],
})