import json


def parse_json(input, output):
    with open(input) as json_file:
        data = json.load(json_file)
        f = open(output, 'w')
        f.write('id,category\n')
        for obj in data:
            print('working!')
            f.write(str(obj['_id']))
            f.write(',')
            f.write(str(obj['Category']))
            f.write('\n')
        
        f.close()
    print('DONE')

if __name__ == '__main__':
    parse_json('data.json', 'init.csv')